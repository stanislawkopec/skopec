﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Exercise_01.Shared;

namespace Exercise_01.Server.Data
{
    public class ExDbContext : DbContext
    {
        public ExDbContext(DbContextOptions<ExDbContext> options) : base(options)
        {
        }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Employee> Employees { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Department>().ToTable("SK_Department");
            modelBuilder.Entity<Employee>().ToTable("SK_Employee");
        }
    }
}
