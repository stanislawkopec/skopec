﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading;
using System.Threading.Tasks;
using Exercise_01.Shared;
namespace Exercise_01.Client.Services
{
    public partial class EmployeeService
    {
        public Uri Base { get; set; }
        async public Task<IEnumerable<Employee>> GetEmployeesEditableAsync(CancellationToken ct = default)
        {
            using (var client = new HttpClient() { BaseAddress = Base })
            {
                return await client.GetFromJsonAsync<IEnumerable<Employee>>("");
            }
        }
        async public Task InsertEmployeeAsync(IDictionary<string, object> newValues)
        {
            using (var client = new HttpClient() { BaseAddress = Base })
            {
                Dictionary<string, string> dString = newValues.ToDictionary(k => k.Key, k => k.Value.ToString());
                await client.PostAsJsonAsync("", newValues);
            }
        }
        async public Task RemoveEmployeeAsync(Employee dataItem)
        {
            using (var client = new HttpClient() { BaseAddress = Base })
            {
                var res = await client.DeleteAsync(client.BaseAddress + "/" + dataItem.ID);
            }
        }
        async public Task UpdateEmployeeAsync(Employee dataItem, IDictionary<string, object> newValues)
        {
            using (var client = new HttpClient() { BaseAddress = Base })
            {
                if (newValues.TryGetValue("DepartmentID", out object Did))
                {
                    dataItem.DepartmentID = Convert.ToInt32(Did);
                }
                if (newValues.TryGetValue("FirstName", out object name))
                {
                    dataItem.FirstName = name.ToString();
                }
                if (newValues.TryGetValue("Surname", out object surname))
                {
                    dataItem.Surname = surname.ToString();
                }
                var res = await client.PutAsJsonAsync(client.BaseAddress + "/" + dataItem.ID, dataItem);
            }
        }
        public Func<CancellationToken, Task<IEnumerable<Employee>>> GetEmployessByDepartmentAsync(Department department)
        {
            return GetEmployeeInviocesAsync;
            async Task<IEnumerable<Employee>> GetEmployeeInviocesAsync(CancellationToken cancellationToken)
            {
                return await GetEmployeesFromIDAsync(department);
            }
        }
        async public Task<IEnumerable<Employee>> GetEmployeesFromIDAsync(Department dataItem)
        {
            IEnumerable<Employee> employ = await GetEmployeesEditableAsync();
            return employ.Where(o => o.DepartmentID == dataItem.ID);
        }
    }
}