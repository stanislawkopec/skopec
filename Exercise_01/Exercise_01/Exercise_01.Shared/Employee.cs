﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Exercise_01.Shared
{
    public class Employee
    {
        [Key]
        public int ID { get; set; }
        [Display(Name = "Id działu pracownika")]
        public int? DepartmentID { get; set; }
        [Display(Name = "Imie pracownika")]
        [Required(ErrorMessage = "Podaj imie pracownika")]
        public string FirstName { get; set; }
        [Display(Name = "Nazwisko pracownika")]
        [Required(ErrorMessage = "Podaj nazwisko pracownika")]
        public string Surname { get; set; }
        [Display(Name = "Dział pracownika")]
        [JsonIgnore]
        public virtual Department? Department { get; set; }
    }
}
