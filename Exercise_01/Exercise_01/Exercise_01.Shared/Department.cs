﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_01.Shared
{
    public class Department
    {
        [Key]
        public int ID { get; set; }
        [Display(Name = "Nazwa działu")]
        [Required(ErrorMessage = "Podaj nazwe działu")]
        public string Name { get; set; }
        [Display(Name = "Lista pracowników działu")]
        public List<Employee> Employess { get; set; }
    }
}